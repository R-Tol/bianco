using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuessVote : MonoBehaviour
{
    public Button vote;

    public Text numberOfVotesText;

    private int numberOfVotes;

    public int NumberOfVotes { get => numberOfVotes; set { numberOfVotes = value; numberOfVotesText.text = numberOfVotes.ToString(); } }

    public Photon.Realtime.Player player;
}
