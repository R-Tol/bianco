using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Linq;
using Photon.Pun.UtilityScripts;

public class GameMasterSelection : MonoBehaviour
{
    [SerializeField]
    int maxDuration;

    PhotonView pv;

    [SerializeField]
    GameObject playerButtonGO;
    [SerializeField]
    GameObject chooseWordPanel;
    [SerializeField]
    GameObject waitMasterPanel;
    [SerializeField]
    DisplayBiancoTeam displayBianco;

    int votingFor = -1;

    [SerializeField]
    int normalPlayersProportion;

    List<ButtonPlayerVotes> candidates = new List<ButtonPlayerVotes>();

    private int numberOfVotes;

    private void Awake()
    {
        pv = GetComponent<PhotonView>();
    }

    private void Start()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            TimerManager.Instance.timer.Countdown = maxDuration;
        }
        CountdownTimer.OnCountdownTimerHasExpired += GoToNextPhase;
    }

    public int NumberOfVotes
    {
        get => numberOfVotes;
        set
        {
            numberOfVotes = value;
            if(PhotonNetwork.IsMasterClient && numberOfVotes == PhotonNetwork.CurrentRoom.PlayerCount)
            {
                //GoNext
            }
        }
    }
    public void ProposeYourself()
    {
        pv.RPC("ComunicateProposal", RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.ActorNumber);
    }

    [PunRPC]
    public void ComunicateProposal(int actorNumber)
    {
        foreach(var player in PhotonNetwork.PlayerList)
        {
            if(player.ActorNumber == actorNumber)
            {
                GeneratePlayerButton(player);
            }
        }
    }

    void GeneratePlayerButton(Photon.Realtime.Player player)
    {

        Button button = Instantiate(playerButtonGO, this.transform).GetComponent<Button>();
        button.transform.GetChild(0).GetComponent<Text>().text = player.NickName;
        candidates.Add(new ButtonPlayerVotes { player = player, button = button, Votes = 0 });
        button.onClick.AddListener(()=>VoteForPlayer(player.ActorNumber));

    }

    public void VoteForPlayer(int actorNumber)
    {
        if (actorNumber == votingFor)
        {
            RemoveVote();
            return;
        }
        pv.RPC("ComunicateVotePlayer", RpcTarget.AllBuffered, actorNumber);
        if (votingFor != -1)
            RemoveVote();
        votingFor = actorNumber;
    }

    public void RemoveVote()
    {
        pv.RPC("ComunicateRemoveVote", RpcTarget.AllBuffered, votingFor);
        votingFor = -1;
    }

    [PunRPC]
    public void ComunicateRemoveVote(int actorNumber)
    {
        foreach (var player in PhotonNetwork.PlayerList)
        {
            if (player.ActorNumber == actorNumber)
            {
                --candidates.Find((x) => x.player.ActorNumber == actorNumber).Votes;
            }
        }
        --numberOfVotes;
    }

    [PunRPC]
    public void ComunicateVotePlayer(int actorNumber)
    {
        foreach (var player in PhotonNetwork.PlayerList)
        {
            if (player.ActorNumber == actorNumber)
            {
                ++candidates.Find((x) => x.player.ActorNumber == actorNumber).Votes;
            }
        }
        ++numberOfVotes;
        if(numberOfVotes == PhotonNetwork.CurrentRoom.PlayerCount)
        {
            GoToNextPhase();
        }
    }

    public void GoToNextPhase()
    {
        CountdownTimer.OnCountdownTimerHasExpired -= GoToNextPhase;
        TimerManager.Instance.timer.Countdown = 0;
        FindGameMaster();
        //ActivateNextSession
    }

    public void FindGameMaster() {
        if (PhotonNetwork.IsMasterClient)
        {
            int gameMaster = PhotonNetwork.CurrentRoom.Players[UnityEngine.Random.Range(1, PhotonNetwork.CurrentRoom.PlayerCount + 1)].ActorNumber;
            if(candidates.Count != 0)
            {
                int mostVotes = -1;
                foreach(var player in candidates)
                {
                    if(mostVotes < player.Votes)
                    {
                        gameMaster = player.player.ActorNumber;
                        mostVotes = player.Votes;
                    }
                }
            }
            pv.RPC("ComunicateGameMaster", RpcTarget.AllBuffered, gameMaster);

            List<int> normalPlayers = new List<int>();

            foreach(var normalPlayer in PhotonNetwork.CurrentRoom.Players.Values)
            {
                if(normalPlayer.ActorNumber != gameMaster)
                {
                    normalPlayers.Add(normalPlayer.ActorNumber);
                }
            }

            GameManager.Instance.startingBiancoPlayersQuantity = normalPlayers.Count / normalPlayersProportion;
            int[] biancoPlayers = new int[GameManager.Instance.startingBiancoPlayersQuantity];
            for (int i = 0; i < GameManager.Instance.startingBiancoPlayersQuantity; i++)
            {
                int rand = UnityEngine.Random.Range(0, normalPlayers.Count);
                biancoPlayers[i] = normalPlayers[rand];
                normalPlayers.RemoveAt(rand);
            }

            pv.RPC("ComunicateBiancoPlayers", RpcTarget.AllBuffered, biancoPlayers);
        }
    }


    [PunRPC]
    public void ComunicateGameMaster(int actorNumber)
    {
        GameManager gm = GameManager.Instance;
        gm.Players.Remove(gm.Players.Find((x) => x.photonView.OwnerActorNr == actorNumber));
        if (actorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            gm.Player.Role = PlayerRole.GameMaster;
           
            chooseWordPanel.SetActive(true);
        }
        else
        {
            waitMasterPanel.SetActive(true);
        }

    }

    [PunRPC]
    public void ComunicateBiancoPlayers(int[] actorNumberss)
    {
        List<int> actorNumbers = actorNumberss.ToList();
        if (actorNumbers.Contains(PhotonNetwork.LocalPlayer.ActorNumber))
        {
            GameManager.Instance.Player.Role = PlayerRole.Bianco;
            actorNumbers.Remove(PhotonNetwork.LocalPlayer.ActorNumber);
            displayBianco.Activate(actorNumbers);
        }
        foreach(int number in actorNumbers)
        {
            GameManager.Instance.biancoPlayers.Add(PhotonNetwork.CurrentRoom.GetPlayer(number));
        }
        
    }

    public class ButtonPlayerVotes
    {
        public Photon.Realtime.Player player;

        public Button button;

        private int votes;

        public int Votes { get => votes; 
            set {
                votes = value;
                if (votes != 0) {
                    button.transform.GetChild(0).GetComponent<Text>().text = player.NickName + " (" + votes.ToString() + ")";
                }
                else
                {
                    button.transform.GetChild(0).GetComponent<Text>().text = player.NickName;
                }
            }
        }
    }
}


