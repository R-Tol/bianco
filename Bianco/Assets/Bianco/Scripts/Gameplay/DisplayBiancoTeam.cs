using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class DisplayBiancoTeam : MonoBehaviour
{
    [SerializeField]
    GameObject playerNamePrefab;
    [SerializeField]
    GameObject panel;

    internal void Activate(List<int> actorNumbers)
    {
        panel.SetActive(true);
        foreach (int actor in actorNumbers)
            Instantiate(playerNamePrefab, transform).GetComponent<Text>().text = PhotonNetwork.CurrentRoom.Players[actor].NickName;
    }
}
