using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
using System.Linq;
using UnityEngine.SceneManagement;

public class RoundManager : Singleton<RoundManager>
{

    Round currentRound;

    public List<Text> hintText;

    int roundNumber = 0;

    public float roundDuration;

    public float biancoExtraTime;

    public GameObject roundPanel;

    public GameObject typeGuessPanel;

    public GameObject guessPanel;

    public GameObject votePanel;

    public GameObject playerNamePrefab;

    public GameObject votePrefab;

    public GameObject endGame;

    public Text guessText;

    int votingFor = -1;

    int numberOfVotes;

    int numberOfGuess;

    PhotonView pv;

    List<GuessVote> candidates = new List<GuessVote>();

    public PopUpDisappearing popUpDisappearing;

    private void Start()
    {
        pv = GetComponent<PhotonView>();
    }

    internal void StartRound(string hintWord)
    {
        roundPanel.SetActive(true);

        ++roundNumber;

        popUpDisappearing.PopUp(1f, "Round" + roundNumber.ToString());

        currentRound = new Round { guesses = new Dictionary<Photon.Realtime.Player, string>(), number = roundNumber, hint = hintWord , eliminatedPlayer=-1};

        if(GameManager.Instance.Player.Role == PlayerRole.Eliminated || GameManager.Instance.Player.Role == PlayerRole.GameMaster)
        {
            typeGuessPanel.SetActive(false);
        }

        foreach(Text text in hintText)
        {
            text.text = hintWord;
        }

        votePanel.SetActive(false);

        candidates = new List<GuessVote>();

        foreach(Transform t in votePanel.transform.GetComponentInChildren<Transform>())
        {
            Destroy(t.gameObject);
        }

        foreach (Transform t in guessPanel.transform.GetComponentInChildren<Transform>())
        {
            Destroy(t.gameObject);
        }
        numberOfGuess = 0;
        numberOfVotes = 0;

        votingFor = -1;

        if (PhotonNetwork.IsMasterClient)
        {
            TimerManager.Instance.StartTimer( roundDuration, "Time remaining: {0}");
        }
            

        CountdownTimer.OnCountdownTimerHasExpired += GiveBiancoExtratime;

        if(GameManager.Instance.Player.Role == PlayerRole.GameMaster)
        {
            HintPanel.Instance.SetupHintPanel();
        }
    }

    public void GiveBiancoExtratime()
    {
        CountdownTimer.OnCountdownTimerHasExpired -= GiveBiancoExtratime;
        if (PhotonNetwork.IsMasterClient)
        {
            TimerManager.Instance.StartTimer(biancoExtraTime, "Bianco extra time: {0}");
        }

        CountdownTimer.OnCountdownTimerHasExpired += SetUpBallot;

    }

    IEnumerator SubscribeToCountdown(Photon.Pun.UtilityScripts.CountdownTimer.CountdownTimerHasExpired action)
    {
        yield return new WaitForSeconds(1.0f);
        CountdownTimer.OnCountdownTimerHasExpired += SetUpBallot;
    }

    public void SetUpBallot()
    {
        CountdownTimer.OnCountdownTimerHasExpired -= SetUpBallot;

        if (GameManager.Instance.Player.Role != PlayerRole.Bianco)
        {
            foreach(var player in GameManager.Instance.Players)
            {
                int actorNumber = player.photonView.Owner.ActorNumber;
                string guess;
                if(currentRound.guesses.TryGetValue(player.photonView.Owner, out guess))
                {
                    CreateGuess(actorNumber, guess);
                }
                else
                {
                    currentRound.guesses.Add(PhotonNetwork.CurrentRoom.GetPlayer( player.photonView.ControllerActorNr), "");
                    CreateGuess(actorNumber, "");
                }
                
            }
        }

        votePanel.SetActive(true);
    }

    public void SendGuess()
    {
        if(guessText.text != "")
        {
            if (GameManager.Instance.Player.Role == PlayerRole.Bianco)
            {
                bool found = false;
                foreach(var value in currentRound.guesses.Values)
                {
                    if(value == guessText.text)
                    {
                        popUpDisappearing.PopUp(2f, "you cannot type an already choosen guess");
                        found = true;
                    }
                }

                if (!found)
                {
                    typeGuessPanel.SetActive(false);
                    //ComunicateGuess(guessText.text, PhotonNetwork.LocalPlayer.ActorNumber);
                    pv.RPC("ComunicateGuess", RpcTarget.AllBuffered, guessText.text, PhotonNetwork.LocalPlayer.ActorNumber);
                }
                    
            }
            else
            {
                typeGuessPanel.SetActive(false);
                pv.RPC("ComunicateGuess", RpcTarget.AllBuffered, guessText.text, PhotonNetwork.LocalPlayer.ActorNumber);
            }
        }
        else
        {
            popUpDisappearing.PopUp(2f, "the string cannot be empty");
        }
    }


    [PunRPC]
    public void ComunicateGuess(string guess, int actorNumber)
    {
        currentRound.guesses.Add(PhotonNetwork.CurrentRoom.Players[actorNumber], guess);
        if(GameManager.Instance.Player.Role == PlayerRole.Bianco)
        {
            CreateGuess(actorNumber, guess);
        }
        
        if(GameManager.Instance.biancoPlayers.Find((x)=>x.ActorNumber == actorNumber) == null){
            ++numberOfGuess;
            
            if (numberOfGuess == GameManager.Instance.GetNoBiancoPlayersAmount())
            {
                
                GiveBiancoExtratime();
            }
                
        }
            
    }


    public void CreateGuess(int actorNumber, string guess)
    {
        PlayerNameGuess png = Instantiate(playerNamePrefab, guessPanel.transform).GetComponent<PlayerNameGuess>();

        png.player.text = PhotonNetwork.CurrentRoom.Players[actorNumber].NickName;

        png.guess.text = guess;

        GuessVote vote = Instantiate(votePrefab, votePanel.transform).GetComponent<GuessVote>();

        vote.player = PhotonNetwork.CurrentRoom.Players[actorNumber];

        candidates.Add(vote);

        vote.NumberOfVotes = 0;

        vote.vote.onClick.AddListener(() => VoteForPlayer(actorNumber));
    }

    public void VoteForPlayer(int actorNumber)
    {
        if (actorNumber == votingFor)
        {
            RemoveVote();
            return;
        }
        pv.RPC("ComunicateVotePlayer", RpcTarget.AllBuffered, actorNumber);
        if (votingFor != -1)
            RemoveVote();
        votingFor = actorNumber;
    }

    public void RemoveVote()
    {
        pv.RPC("ComunicateRemoveVote", RpcTarget.AllBuffered, votingFor);
        votingFor = -1;
    }

    [PunRPC]
    public void ComunicateRemoveVote(int actorNumber)
    {
        foreach (var player in candidates)
        {
            if (player.player.ActorNumber == actorNumber)
            {
                --player.NumberOfVotes;
            }
        }
        --numberOfVotes;
    }

    [PunRPC]
    public void ComunicateVotePlayer(int actorNumber)
    {
        
        foreach (var player in candidates)
        {
            if (player.player.ActorNumber == actorNumber)
            {
                ++player.NumberOfVotes;
            }
        }
        ++numberOfVotes;
        if (numberOfVotes == PhotonNetwork.CurrentRoom.PlayerCount)
        {
            EliminatePlayer();
        }
    }

    public void EliminatePlayer()
    {
        GameManager gm = GameManager.Instance;
        int mostVotes = 0;
        var mostVoted = candidates[0].player;
        foreach (var candidate in candidates)
        {
            if (candidate.NumberOfVotes > mostVotes)
            {
                mostVotes = candidate.NumberOfVotes;
                mostVoted = candidate.player;
            }
        }
        if (mostVotes > 0)
        {
            gm.Players.Remove(gm.Players.Find((x) => x.photonView.OwnerActorNr == mostVoted.ActorNumber));
            currentRound.eliminatedPlayer = mostVoted.ActorNumber;
        }
        if(GameManager.Instance.Player.photonView.OwnerActorNr == mostVoted.ActorNumber)
        {
            GameManager.Instance.Player.Role = PlayerRole.Eliminated;
        }
        GameManager.Instance.pastRounds.Add(currentRound);
        
        StartCoroutine(ComunicateEliminatedPlayerAndStartNextRound(mostVoted.NickName));
    }

    IEnumerator ComunicateEliminatedPlayerAndStartNextRound(string eliminated)
    {
        if(GameManager.Instance.biancoPlayers.Find((x)=> x.ActorNumber == currentRound.eliminatedPlayer) != null)
            popUpDisappearing.PopUp(3f, eliminated + "has been eliminated! \n" + "Their Role was Bianco!");
        else
            popUpDisappearing.PopUp(3f, eliminated + "has been eliminated! \n" + "Their Role was not Bianco!");
        if (GameManager.Instance.biancoPlayers.Find((x) => x.ActorNumber == currentRound.eliminatedPlayer) != null)
            GameManager.Instance.biancoPlayers.Remove(GameManager.Instance.biancoPlayers.Find((x) => x.ActorNumber == currentRound.eliminatedPlayer));
        
        if(GameManager.Instance.Players.Count <= GameManager.Instance.startingBiancoPlayersQuantity + 1)
        {
            string winner = "COLOR";

            foreach(Player player in GameManager.Instance.Players)
            {
                foreach(var bianco in GameManager.Instance.biancoPlayers)
                {
                    if (player.photonView.CreatorActorNr == bianco.ActorNumber)
                        winner = "BIANCO";
                }
            }
            yield return new WaitForSeconds(3f);
            EndGame(winner);
        }
        else
        {
            yield return new WaitForSeconds(3f);
            StartRound(GameManager.Instance.NextHint);
        }
        
    }

    public void EndGame(string winner)
    {
        endGame.SetActive(true);
        endGame.transform.GetComponentInChildren<Text>().text = "THE WINNER IS " + winner + " TEAM!!!";
    }

    public void Exit()
    {
        SceneManager.LoadScene("BiancoLobby");
        PhotonNetwork.LeaveRoom();
    }



}
