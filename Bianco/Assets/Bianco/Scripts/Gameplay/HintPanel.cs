using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HintPanel : Singleton<HintPanel>
{
    public Text text;

    public Text showText;

    public void SetupHintPanel()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        GameManager.Instance.NextHint = "";
    }

    public void SetNextHint()
    {
        GameManager.Instance.NextHint = text.text;
        showText.text = "NEXT HINT CHOOSEN; \n" + text.text;
    }

}
