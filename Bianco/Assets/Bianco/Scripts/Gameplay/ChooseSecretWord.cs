using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;

public class ChooseSecretWord : MonoBehaviour
{
    [SerializeField]
    InputField secretWord;
    [SerializeField]
    InputField firsthint;

    public PopUpDisappearing emptyStringFound;

    PhotonView photonView;


    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    public void SelectWord()
    {
        if(secretWord.text != "" && firsthint.text != "")
        {
            string[] wordHint = { secretWord.text, firsthint.text };
            photonView.RPC("ComunicateWordAndHint", RpcTarget.AllBuffered, wordHint);
        }
        else
        {
            emptyStringFound.PopUp(2f);
        }
        
        
    }

    [PunRPC]
    public void ComunicateWordAndHint(string[] wordHint)
    {
        Debug.Log(wordHint[0] + wordHint[1]);
        GameManager.Instance.ComunicateSecretWord(wordHint[0]);
        RoundManager.Instance.StartRound(wordHint[1]);
    }




}
