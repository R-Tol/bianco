using Photon.Pun.UtilityScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class TimerManager : Singleton<TimerManager>
{
    public CountdownTimer timer;

    public PhotonView photonView;

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
    }

    public void StartTimer(float seconds, string textToType)
    {
        photonView.RPC("RPCStartTimer", RpcTarget.AllBufferedViaServer, seconds, textToType);
    }

    [PunRPC]
    public void RPCStartTimer(float seconds, string textToType)
    {
        
        timer.Countdown = seconds;
        timer.enabled = true;
        timer.textToType = textToType;
        if(PhotonNetwork.IsMasterClient)
            CountdownTimer.SetStartTime();
    }

}
