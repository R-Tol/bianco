using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpDisappearing : MonoBehaviour
{

    public Image image;

    public Text text;


    public void PopUp(float showUpTime)
    {
        gameObject.SetActive(true);
        StartCoroutine(ApperDisappearEffect(showUpTime));
    }

    public void PopUp(float showUpTime, string text)
    {
        gameObject.SetActive(true);
        this.text.text = text;
        StartCoroutine(ApperDisappearEffect(showUpTime));
    }


    IEnumerator ApperDisappearEffect(float showUpTime)
    {
        float disappearingDuration = showUpTime / 5;

        float timePassed = 0;

        while (timePassed < disappearingDuration)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, timePassed / disappearingDuration);
            text.color = new Color(text.color.r, text.color.g, text.color.b, timePassed / disappearingDuration);

            timePassed += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(showUpTime / 5 * 3);

        timePassed = 0;

        while (timePassed < disappearingDuration)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, ( disappearingDuration - timePassed ) / disappearingDuration);
            text.color = new Color(text.color.r, text.color.g, text.color.b, (disappearingDuration - timePassed) / disappearingDuration);

            timePassed += Time.deltaTime;
            yield return null;
        }

        gameObject.SetActive(false);
    }


}
