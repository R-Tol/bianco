using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class GameManager : Singleton<GameManager>
{
    private Player player;
    private List<Player> players = new List<Player>();

    private string secretWord;

    public Player Player { get => player; set => player = value; }
    public List<Player> Players { get => players; set => players = value; }

    public List<Photon.Realtime.Player> biancoPlayers = new List<Photon.Realtime.Player>();

    public string SecretWord { get => secretWord; }
    public string NextHint { get => nextHint; set { pv.RPC("SetNextHint", RpcTarget.AllBuffered, value); } }

    public List<Round> pastRounds = new List<Round>();

    public List<Text> secretWordText;

    private string nextHint;

    public PhotonView pv;

    public int startingBiancoPlayersQuantity;

    private void Start()
    {
        pv = GetComponent<PhotonView>();
    }

    [PunRPC]
    void SetNextHint(string hint)
    {
        nextHint = hint;
    }

    public Photon.Realtime.Player PlayerToPunPlayer(Player player)
    {
        return PhotonNetwork.CurrentRoom.GetPlayer(player.photonView.ControllerActorNr);
    }

    public Player PunPlayerToPlayer(Photon.Realtime.Player punPlayer)
    {
        foreach(Player player in Players)
        {
            if (player.photonView.OwnerActorNr == punPlayer.ActorNumber)
                return player;
        }
        return null;
    }

    internal void ComunicateSecretWord(string secretWord)
    {
        this.secretWord = secretWord;
        if(player.Role != PlayerRole.Bianco)
        {
            foreach(Text text in secretWordText)
            {
                text.text = secretWord;
            }
        }
        else
        {
            foreach (Text text in secretWordText)
            {
                text.text = "bianco";
            }
        }
    }

    public int GetNoBiancoPlayersAmount()
    {
        int amount = 0;
        foreach(Player player in Players)
        {
            bool isBianco = false;
            foreach(var bianco in biancoPlayers)
            {
                if (player.photonView.OwnerActorNr == bianco.ActorNumber)
                {
                    isBianco = true;
                    break;
                }
            }
            if (!isBianco)
                amount++;
        }
        return amount;
    }
}

public class Round 
{
    public string hint;
    public Dictionary<Photon.Realtime.Player, string> guesses;
    public int eliminatedPlayer;
    public int number;
}
