using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class Player : MonoBehaviour
{
    public PhotonView photonView;

    private PlayerRole role;

    public PlayerRole Role { get => role;
        set {
            role = value;
        }
    }

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if(photonView.AmOwner && photonView.AmController)
        {
            GameManager.Instance.Player = this;
        }
        
        GameManager.Instance.Players.Add(this);

    }

    

}
